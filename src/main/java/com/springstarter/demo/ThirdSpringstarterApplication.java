package com.springstarter.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// TODO: Auto-generated Javadoc
/**
 * The Class ThirdSpringstarterApplication.
 */
@SpringBootApplication
public class ThirdSpringstarterApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ThirdSpringstarterApplication.class, args);
	}

}
