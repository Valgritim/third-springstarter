package com.springstarter.demo.controller;

import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springstarter.demo.exceptions.ResourceNotFoundException;
import com.springstarter.demo.model.Personne;
import com.springstarter.demo.services.PersonneService;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonneRestController.
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class PersonneRestController {

	/** The personne service. */
	@Autowired
	private PersonneService personneService;

	/**
	 * Va récupérer la liste des personnes dans la BD
	 *
	 * @return the personnes
	 */
	@GetMapping(value = "/personnes")
	public Collection<Personne> getPersonnes() {
		Collection<Personne> personnes = personneService.getAllPersonne();
		return personnes;
	}

	/**
	 * Va chercher une personne  par son id dans la BD
	 *
	 * @param id the id
	 * @return the personne by id
	 * @throws ResourceNotFoundException Quand la personne n'a pas été trouvée
	 */
	@GetMapping(value = "/personnes/{id}")
	public ResponseEntity<Personne> getPersonneById(@PathVariable Long id) {

		Personne personne = personneService.getPersonneById(id);
		if (personne == null) {

			throw new ResourceNotFoundException("Personne not found : " + id);
		}

		return ResponseEntity.ok().body(personne);

	}

	/**
	 * Save.
	 *
	 * @param personne créationd de l'objet personne dans la BD
	 * @return the personne
	 */
	@PostMapping(value = "/personnes", produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public Personne save(@Valid @RequestBody Personne personne) {

		return personneService.saveOrUpdatePersonne(personne);

	}

	/**
	 * Update personne.
	 *
	 * @param id the id
	 * @param personne the personne
	 * @return the response entity
	 * @throws ResourceNotFoundException Quand la personne n'a pas été trouvée
	 *
	 */
	@PutMapping("/personnes/{id}")
	@Transactional
	public ResponseEntity<Personne> updatePersonne(@PathVariable(value = "id") Long id,
			@RequestBody Personne personne) {
		Personne personneToUpdate = personneService.getPersonneById(id);
		if (personneToUpdate == null) {
			throw new ResourceNotFoundException("Personne not found : " + id);
		}
		personneToUpdate.setNom(personne.getNom());
		personneToUpdate.setPrenom(personne.getPrenom());
		Personne personneUpdated = personneService.saveOrUpdatePersonne(personneToUpdate);
		return new ResponseEntity<Personne>(personneUpdated, HttpStatus.OK);
	}

	/**
	 * Delete personne.
	 *
	 * @param id the id
	 * @throws ResourceNotFoundException Quand la personne n'a pas été trouvée
	 */	 
	@DeleteMapping("/personnes/{id}")
	@Transactional
	public void deletePersonne(@PathVariable(value = "id") Long id) {
		Personne personne = personneService.getPersonneById(id);
		if (personne == null) {
			throw new ResourceNotFoundException("Personne not found : " + id);
		}

		personneService.deletePersonne(id);
	}

}
			
			

