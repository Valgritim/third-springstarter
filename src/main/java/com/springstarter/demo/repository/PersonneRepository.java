package com.springstarter.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;


import com.springstarter.demo.model.Personne;


// TODO: Auto-generated Javadoc
/**
 * The Interface PersonneRepository.
 */
public interface PersonneRepository extends JpaRepository<Personne, Long>{	
	
	/**
	 * Find by num.
	 *
	 * @param num the num
	 * @return the personne
	 */
	Personne findByNum(Long num);
}
