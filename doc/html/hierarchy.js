var hierarchy =
[
    [ "com.springstarter.demo.PersonneRepositoryTest", "classcom_1_1springstarter_1_1demo_1_1_personne_repository_test.html", null ],
    [ "com.springstarter.demo.controller.PersonneRestController", "classcom_1_1springstarter_1_1demo_1_1controller_1_1_personne_rest_controller.html", null ],
    [ "com.springstarter.demo.services.PersonneService", "interfacecom_1_1springstarter_1_1demo_1_1services_1_1_personne_service.html", [
      [ "com.springstarter.demo.services.PersonneServiceImpl", "classcom_1_1springstarter_1_1demo_1_1services_1_1_personne_service_impl.html", null ]
    ] ],
    [ "com.springstarter.demo.PersonneServiceTest", "classcom_1_1springstarter_1_1demo_1_1_personne_service_test.html", null ],
    [ "RuntimeException", null, [
      [ "com.springstarter.demo.exceptions.ResourceNotFoundException", "classcom_1_1springstarter_1_1demo_1_1exceptions_1_1_resource_not_found_exception.html", null ]
    ] ],
    [ "com.springstarter.demo.ThirdSpringstarterApplication", "classcom_1_1springstarter_1_1demo_1_1_third_springstarter_application.html", null ],
    [ "JpaRepository", null, [
      [ "com.springstarter.demo.repository.PersonneRepository", "interfacecom_1_1springstarter_1_1demo_1_1repository_1_1_personne_repository.html", null ]
    ] ],
    [ "Serializable", null, [
      [ "com.springstarter.demo.dto.PersonneDto", "classcom_1_1springstarter_1_1demo_1_1dto_1_1_personne_dto.html", null ],
      [ "com.springstarter.demo.model.Personne", "classcom_1_1springstarter_1_1demo_1_1model_1_1_personne.html", null ]
    ] ]
];